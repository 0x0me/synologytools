#!/bin/sh

# File downloads all new episodes from the german public television mediatheks
# whiach are containing the string 'mia and me'.
#
# This file uses the great service from mediathekdirekt.de for data provision.
# Thanks for your great work!

# host to write syslog messages to
loghost='localhost'

# temporary file to save the list of tv data
dlist='/tmp/mediathek.dlist'

# directory where to put our downloaded files
outputDir='/tmp/mia'

logger -n $loghost "==mia and me downloader== started..."

# create directory structure (no error if already there)
mkdir -p $outputDir

# get updated data list
wget -O $dlist http://www.mediathekdirekt.de/good.json

# filter
candidates=$(cat $dlist | sed s/]/\\n/g | grep -i "mia and me" | awk -F '", "' '{print($7)}' )


for ca in $candidates
do
  path=$(dirname $ca)

  file=$(echo $ca | sed "s,$path/,,g")

  # try to check if we already had downloaded the file
  if [ -f "$outputDir/$file" ]
  then
    echo "Skipping $ca - already exists"
	logger -n $loghost "==mia and me downloader== skipping '$ca'"
  else
	wget -P $outputDir $ca
	logger -n $loghost "==mia and me downloader== download of ''$ca' finished"
  fi
done
logger -n $loghost "==mia and me downloader== finished!"
