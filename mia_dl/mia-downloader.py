#!/usr/local/bin/python3
import json
import os.path
import urllib.request
import syslog
import math

output_dir = '/tmp/mia'


def query():
    """
    Queries the mediathekviewweb project for tv series records of 'mia and me' using the api found at
    'https://mediathekviewweb.de/api/query'. The request has to be a POST request using 'text/plain' as
    content type.

    :return: json of search results
    """
    request = urllib.request.urlopen(urllib.request.Request(url='https://mediathekviewweb.de/api/query',
                                                            data='{"queries":['
                                                                 '{"fields":["topic"],"query":"mia"}'
                                                                 '],"sortBy":"timestamp",'
                                                                 '"sortOrder":"desc",'
                                                                 '"future":false,'
                                                                 '"offset":0,'
                                                                 '"size":15'
                                                                 '}'.encode('UTF-8'),
                                                            headers={'Content-Type': 'text/plain'}
                                                            ))

    response = request.read()
    encoding = request.info().get_content_charset('utf-8')
    data = json.loads(response.decode(encoding))
    results = data['result']['results']

    return results


def download(data):
    """
    Checks and downloads the video file if it is not already present.
    :param data: json data structure having the fields 'title', 'size', 'url_video'
    :return: Option
    """
    global output_dir
    print("Downloading " + data['title'] + " ~ " + str(math.ceil(data['size'] / (1024 * 1024))) + "MB")
    url = data['url_video']
    file_name = os.path.basename(url)
    print("Filename is " + file_name)

    # check if the file already exists
    output_file = output_dir + os.path.sep + file_name
    if os.path.isfile(output_file):
        print("Skipping file, already exists")
        syslog.syslog("Skipping " + url + ", because " + output_file + " already exists")
        return None

    print("downloading file...")
    syslog.syslog("Downloading " + url + "to " + output_file + " ... ")
    urllib.request.urlretrieve(url=url, filename=output_file)
    syslog.syslog("Download from " + url + "to " + output_file + "finished")

    return output_file


"""
Regular script starts here
"""

if not os.path.exists(output_dir):
    syslog.syslog("Output dir " + output_dir + " does not exists, creating...")
    os.makedirs(output_dir)
    syslog.syslog("Output dir " + output_dir + "created!")

syslog.syslog("Starting mia-downloader.py")
# use for comprehension to filter all results under certain duration to filter out specials and shorts
[download(r) for r in query() if r['duration'] > 1000]

syslog.syslog("Finished!")
