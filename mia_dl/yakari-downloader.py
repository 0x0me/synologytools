#!/usr/local/bin/python3
import json
import os.path
import urllib.request
import syslog
import math
import re


output_dir = '/tmp/mia'
# output_dir = '/volume1/video/Serien/Yakari'


def query():
    """
    Queries the mediathekviewweb project for tv series records of 'Yakari' using the api found at
    'https://mediathekviewweb.de/api/query'. The request has to be a POST request using 'text/plain' as
    content type.

    :return: json of search results
    """
    request = urllib.request.urlopen(urllib.request.Request(url='https://mediathekviewweb.de/api/query',
                                                            data='{"queries":['
                                                                 '{"fields":["topic"],"query":"yakari"}'
                                                                 '],"sortBy":"timestamp",'
                                                                 '"sortOrder":"desc",'
                                                                 '"future":false,'
                                                                 '"offset":0,'
                                                                 '"size":15'
                                                                 '}'.encode('UTF-8'),
                                                            headers={'Content-Type': 'text/plain'}
                                                            ))

    response = request.read()
    encoding = request.info().get_content_charset('utf-8')
    data = json.loads(response.decode(encoding))
    results = data['result']['results']

    return results


def download(data):
    """
    Checks and downloads the video file if it is not already present.
    :param data: json data structure having the fields 'title', 'size', 'url_video'
    :return: Option
    """
    global output_dir
    print("Downloading " + data['title'] + " ~ " + str(math.ceil(data['size'] / (1024 * 1024))) + "MB")
    url = data['url_video_hd']

    if not url:
        print("No HD - try url_video")
        url = data['url_video']

    if not url:
        print("Neither HD nor url_video - try url_video_low")
        url = data['url_video_low']

    file_name = get_valid_filename(data['title']) + ".mp4"
    print("Filename is " + file_name)

    # check if the file already exists
    output_file = output_dir + os.path.sep + file_name
    if os.path.isfile(output_file):
        print("Skipping file, already exists")
        syslog.syslog("Skipping " + url + ", because " + output_file + " already exists")
        return None

    print("downloading file...")
    syslog.syslog("Downloading " + url + "to " + output_file + " ... ")
    urllib.request.urlretrieve(url=url, filename=output_file)
    syslog.syslog("Download from " + url + "to " + output_file + "finished")

    return output_file


def get_valid_filename(s):
    """
    From django https://github.com/django/django/blob/master/django/utils/text.py


    Return the given string converted to a string that can be used for a clean
    filename. Remove leading and trailing spaces; convert other spaces to
    underscores; and remove anything that is not an alphanumeric, dash,
    underscore, or dot.
    >>> get_valid_filename("john's portrait in 2004.jpg")
    'johns_portrait_in_2004.jpg'
    """
    s = str(s).strip().replace(' ', '_')
    s = s.replace('.', '_')
    # s = s.replace(':', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)


"""
Regular script starts here
"""

if not os.path.exists(output_dir):
    syslog.syslog("Output dir " + output_dir + " does not exists, creating...")
    os.makedirs(output_dir)
    syslog.syslog("Output dir " + output_dir + "created!")

syslog.syslog("Starting yakari-downloader.py")
# use for comprehension to filter all results under certain duration to filter out specials and shorts
[download(r) for r in query() if r['duration'] > 700]

syslog.syslog("Finished!")
